package icsaszar.scalaspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScalaSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScalaSpringBootApplication.class, args);
    }
}
