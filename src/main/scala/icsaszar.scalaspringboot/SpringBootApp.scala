package icsaszar.scalaspringboot

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SpringBootApp {}

object SpringBootApp{
    def main(args: Array[String]): Unit = {
        SpringApplication.run(classOf[SpringBootApp], args: _*)
    }
}
