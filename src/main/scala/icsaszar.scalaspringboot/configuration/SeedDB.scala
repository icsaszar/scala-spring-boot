package icsaszar.scalaspringboot.configuration

import java.time.LocalDate

import icsaszar.scalaspringboot.models.Movie
import icsaszar.scalaspringboot.repositories.MovieRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.{Bean, Configuration}

@Configuration
class SeedDB {
  @Bean
  def initDatabase(repository: MovieRepository): CommandLineRunner = (args: Array[String]) => {
    def seed(args: Array[String]) : Unit = {
      repository.save(Movie("Inception",
        "PG-13",
        "A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a CEO.",
        LocalDate.of(2010,7,16)))
      repository.save(Movie("Interstellar",
        "PG-13",
        "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.",
        LocalDate.of(2014,11,7)))
      repository.save(Movie("Dunkirk",
        "PG-13",
        "Allied soldiers from Belgium, the British Empire and France are surrounded by the German Army, and evacuated during a fierce battle in World War II.",
        LocalDate.of(2017,8,21)))
    }

    seed(args)
  }
}