package icsaszar.scalaspringboot

import icsaszar.scalaspringboot.controllers.MovieController
import icsaszar.scalaspringboot.models.Movie
import org.springframework.hateoas.{Resource, ResourceAssembler}
import org.springframework.hateoas.mvc.ControllerLinkBuilder._
import org.springframework.stereotype.Component

@Component
class MovieResourceAssembler extends ResourceAssembler[Movie, Resource[Movie]]{
  override def toResource(movie: Movie): Resource[Movie] =
    new Resource[Movie](movie,
      linkTo(methodOn(classOf[MovieController]).one(movie.id)).withSelfRel(),
      linkTo(methodOn(classOf[MovieController]).editMovieForm(movie.id)).withRel("edit"),
      linkTo(methodOn(classOf[MovieController]).destroyMovie(movie.id)).withRel("delete"),
      linkTo(methodOn(classOf[MovieController]).all()).withRel("movies"))
}
