package icsaszar.scalaspringboot.models

import java.time.LocalDate

import javax.persistence.{Entity, GeneratedValue, Id}
import org.springframework.format.annotation.DateTimeFormat

import scala.annotation.meta.field
import scala.beans.BeanProperty

@Entity
case class Movie (
  @BeanProperty
  var title: String,
  @BeanProperty
  var rating: String,
  @BeanProperty
  var description: String,
  @(DateTimeFormat @field)(pattern = "yyyy-MM-dd")
  @BeanProperty
  var released: LocalDate
  ) {

  @Id @GeneratedValue
  var id : Long = _

  def this() {
    this("","","",LocalDate.now())
  }
}
