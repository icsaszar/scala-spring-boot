package icsaszar.scalaspringboot.utils

class MovieNotFoundException(id : Long) extends RuntimeException("Movie with ID: " + id + " not found!")