package icsaszar.scalaspringboot.utils

import java.sql.Date
import java.time.LocalDate

import javax.persistence.{AttributeConverter, Converter}

@Converter(autoApply = true)
class LocalDateAttributeConverter extends AttributeConverter[LocalDate, Date]{
  override def convertToDatabaseColumn(localDate: LocalDate): Date =
    if (localDate == null)
      null
    else Date.valueOf(localDate)

  override def convertToEntityAttribute(date: Date): LocalDate =
    if (date == null)
      null
    else date.toLocalDate
}
