package icsaszar.scalaspringboot.repositories

import icsaszar.scalaspringboot.models.Movie
import org.springframework.data.jpa.repository.JpaRepository

trait MovieRepository extends JpaRepository[Movie, Long] {


}
