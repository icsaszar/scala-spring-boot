package icsaszar.scalaspringboot.advice

import icsaszar.scalaspringboot.utils.MovieNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.{ControllerAdvice, ExceptionHandler, ResponseBody, ResponseStatus}

@ControllerAdvice
class MovieNotFoundAdvice {
  @ResponseBody
  @ExceptionHandler(Array(classOf[MovieNotFoundException]))
  @ResponseStatus(HttpStatus.NOT_FOUND)
  def movieNotFoundHandler(ex: MovieNotFoundException) : String = ex.getMessage
}
