package icsaszar.scalaspringboot.controllers

import scala.collection.JavaConverters._
import icsaszar.scalaspringboot.MovieResourceAssembler
import icsaszar.scalaspringboot.models.Movie
import icsaszar.scalaspringboot.repositories.MovieRepository
import icsaszar.scalaspringboot.utils.MovieNotFoundException
import org.springframework.hateoas.Resource
import org.springframework.web.bind.annotation._
import org.springframework.web.servlet.ModelAndView

import scala.collection.immutable.HashMap

@RestController
class MovieController(repository: MovieRepository, assembler : MovieResourceAssembler) {

//  @GetMapping(Array("/movies"))
//  def all() : java.util.List[Movie] = {
//    repository.findAll()
//  }

  @GetMapping(Array("/movies"))
    def all() :  ModelAndView = {

     var movies : java.util.List[Resource[Movie]] = repository.findAll()
       .asScala
       .map(assembler.toResource)
       .asJava

      new ModelAndView("movies", HashMap("movies" -> movies).asJava)
    }

  @PostMapping(Array("/movies"))
  def newMovie(@ModelAttribute("movie") movie: Movie) : ModelAndView = {
    repository.save(movie)
    new ModelAndView("redirect:/movies")
  }

  @GetMapping(Array("/movies/new"))
  def newMovieForm() : ModelAndView =
    new ModelAndView("new_movie", HashMap("movie" -> new Movie()).asJava)


  @GetMapping(Array("/movies/{id}/edit"))
  def editMovieForm(@PathVariable id : java.lang.Long) : ModelAndView = {
    val movie : Movie = repository.findById(id).orElse(new Movie())
    new ModelAndView("edit_movie", HashMap("movie" -> movie, "id" -> id).asJava)
  }

  @GetMapping(Array("/movies/{id}"))
  def one(@PathVariable id : Long) : ModelAndView ={
    val movie = repository.findById(id)
      .orElseThrow(() => new MovieNotFoundException(id))

    new ModelAndView("movie", HashMap("movie" -> assembler.toResource(movie)).asJava)
  }

  @PostMapping(Array("/movies/{id}"))
  def updateMovie(@ModelAttribute("movie") movie: Movie, @PathVariable id : Long) : ModelAndView = {
    repository.findById(id)
      .map(m => {
        m.title = movie.title
        m.rating = movie.rating
        m.description = movie.description
        m.released = movie.released
        repository.save(m)
      })
      .orElseGet(() => {
        movie.id = id
        repository.save(movie)
      })
    new ModelAndView("redirect:/movies")
  }


  @DeleteMapping(Array("/movies/{id}"))
  def deleteMovie(@PathVariable id : Long) : Unit = repository.deleteById(id)

  @PostMapping(Array("/movies/{id}/delete"))
  def destroyMovie(@PathVariable id : Long) : ModelAndView = {
    repository.deleteById(id)
    new ModelAndView("redirect:/movies")
  }
}
